# copy-cat

probably going to rename this, it's a very silly name.

## what's this?

atm we have very very WIP instruction decoder for building emulators with

the idea is that you define the architecture in an alist format and then
you read it and you get a bunch of classes for each instruction and lots of information
that you can exploit for writing disassemblers/interpreters/decompilers/idk whatever.

## what's the LICENSE

it's licensed under:

Cooperative Non-Violent Public License v4+ (CNPLv4+)
https://git.pixie.town/thufie/CNPL

but I would also like to talk to you about this if you're interested.
