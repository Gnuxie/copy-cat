## metaobject

last updated 2020-05-12

```
(defclass metaobject-reference-class (c2mop:standard-class)
  ((%metaobject :initarg :metaobject :reader metaobject)))
```

we do this and bootstrap the references in the load env so that when an instruction is read by the itnerperter/decompiler or disassembler it is read into an instance that you can define methods for, so your instruction becomes an instance of the instruction class and you can do whatever you want


that didn't make much sense, basically

An interpreter reads an int -> it gets wacked into this from the disaptch tree:

```
	   (MAKE-INSTANCE 'IXF033 :ARGUMENTS
			  (LIST (LDB '(4 . 15) CC.ID:INSTRUCTION))
			  :RAW-INSTRUCTION CC.ID:INSTRUCTION))
```

then if you did write an interpreter all you would need to do is do
```
(defmethod my-rubbish-interpreter-dispatch ((machine machine) (instruction IXF033))
 ...)
```

﻿please tell me the metaobject-reference-class is very silly (basically i didn't explain that yet but that links to one of these things

```
(defclass instruction (debug-info)
  ((%composition :initarg :composition :accessor composition :json-key "composition")
   (%arguments :initarg :arguments :accessor arguments :initform (list) :type list
               :documentation "Association list of (byte-spec . argument)")
   (%effects :initarg :effects :accessor effects :initform (list)
                  :type list :documentation "Any registers that the instruction
will effect as a result of its operation that are not included in the arguments.")
   (%size :initarg :size :accessor size :type (unsigned-byte 16))
   (%constant-mask :initarg :constant-mask :accessor constant-mask
                       :documentation"
1 for constant, 0 for argument, unnormalized for size.
e.g. for a composition '((4 2) (\"u12\" 12)) => 1111000000000000")
   (%constant-value :initarg :constant-value :accessor constant-value
                       :documentation "
If there is a constant value for this then put it in the output, otherwise 0
e.g.'((4 2) (\"u12\" 12)) => 0010000000000000
See constant-mask")
   (%finalized :reader finalized :type boolean :initform nil
               :documentation "Whether the composition and instruction
has been finalized"))
  (:metaclass jcsc:json-serializable-class))
```

(this is meta so the arguments in this class are formal arguments like 'this is always a label for one of these registers') (this is useful for idk the register-allocation anlysis later on when jit time)

to be absoloutley clear the class of the instruction `'IXF033` is an instance of the metaclass `metaobject-reference-class` that contains a reference to an instance of the `instruction` class (which contains meta info about the instruction `#xF033`)

and and instance of`IXF033` is supposed to be a live instruction with operands that's just been read in fake memory or in a disassembled program

`instruction` is not the right name for this class and it needs to be renamed `instruction-metaobject` but that's not going to be very easy now
