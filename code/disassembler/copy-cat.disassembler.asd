(asdf:defsystem #:copy-cat.disassembler
  :description "nothing"
  :license "Cooperative Non-Violent Public License v4+"
  :depends-on ("copy-cat.instruction-database" "copy-cat.cpu"
                                               "copy-cat.machine-introspection"
                                               "nice-directory-explorer"
                                               "mcclim")
  :components ((:file "package")
               (:file "instruction-list")))
