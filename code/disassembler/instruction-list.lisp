#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:copy-cat.disassembler)

(define-presentation-method present (instruction (type cc.id:standard-instruction)
                                            stream view &key)
  (declare (ignore view))
  (cc.id::write-disassembled-instruction instruction stream))

(define-presentation-method present (memory-address (type cc.mi:memory-address)
                                                    stream view &key)
  (declare (ignore view))
  (format stream "~X" (cc.mi:address memory-address)))

(define-application-frame disassembler ()
  ((current-standard-instruction :initform nil)
   (current-disassembely :initform #()
                         :accessor current-disassembely)
   (interaction-pane)
   (disassembely-pane))
  (:pointer-documentation t)
  (:panes (interactor :interactor)
          (directory-list
           (make-pane 'nde:directory-list
                      :display-function 'nde:display-directory
                      :current-directory (asdf:system-relative-pathname
                                          '#:copy-cat.disassembler
                                          "")))
          (disassembley :application
                        :incremental-redisplay t
                        :display-function 'display-standard-instruction))
  (:layouts
   (default
       (horizontally ()
         (vertically ()
           (scrolling (:scroll-bars :vertical)
             directory-list)
           interactor)
         disassembley))))

(defun largest-address-length (addresses)
  "this is shit but can't think rn"
  (if (or (not addresses) (= 0 (length addresses)))
      0
      (length
       (format nil "~X" (cc.mi:address
                         (aref addresses (1- (length addresses))))))))

(defun display-address-address-table (stream addresses)
  (with-text-family (stream :fix)
    (let ((table-record
           (bounding-rectangle
            (formatting-table (stream :x-spacing 10 :y-spacing 5)
              (let ((address-print-width (largest-address-length addresses)))
                (loop :for memory-address :across addresses
                   :do (formatting-row (stream)
                         (surrounding-output-with-border (stream :background +cyan+)
                           (formatting-cell (stream)
                             (format stream "~V,'0X" address-print-width
                                     (cc.mi:address memory-address))))
                         (formatting-cell (stream)
                           (format stream "~2,'0X" (cc.mi:raw-byte memory-address))))))))))
      (values (rectangle-width table-record)
              (if (= 0 (length addresses))
                  0
                  (/ (rectangle-height table-record) (length addresses)))))))

(defun draw-boxed-text (stream text box-height
                        &rest box-options
                        &key (x-spacing 5)
                          &allow-other-keys)
  (multiple-value-bind (cx cy) (stream-cursor-position stream)
    (with-translation (stream cx cy)
      (let* ((text-middle (/ box-height 2))
             (text-record
              (with-output-to-output-record (stream)
                (draw-text* stream text x-spacing text-middle
                            :align-x :left :align-y :center))))
        (let ((box-width
               (+ (+ 2 x-spacing)
                  (rectangle-width text-record))))
          (apply #'draw-rectangle* stream 0 0 box-width box-height
                 box-options)
          (stream-add-output-record stream text-record)
          (replay text-record stream)
          (values box-width box-height))))))

(defun display-interpretation (stream row-height interpretation)
  (let ((text
         (with-output-to-string (s)
           (cc.id::write-mnemonic-and-args interpretation s)))
        (box-height (* row-height (floor (cc.id:size (cc.id:metaobject interpretation))
                                         8))))
    (let ((box-width
           (draw-boxed-text stream text box-height
                            :filled nil :line-dashes t)))
      (values box-width box-height))))

(defmethod display-standard-instruction ((frame disassembler) stream)
  (multiple-value-bind (row-width row-height)
      (display-address-address-table stream (current-disassembely frame))
    (let ((column-empty-p nil)
          (column-width row-width)
          (printed-table (make-hash-table)))
      (loop :until column-empty-p
         :for column :from 0
         :do (setf column-empty-p t)
         :do (let* ((previous-interpretation-size 0) ; should be remaining.
                    (column-output-record
                     (with-output-to-output-record (stream)
                       (loop :for row :from 0
                          :below (length (current-disassembely frame))
                          :for memory-address :across (current-disassembely frame)
                          :do (stream-set-cursor-position stream column-width
                                                          (* row row-height))
                          :do (let ((interpretation
                                     (loop :for interpretation
                                        :across (cc.mi:interpretations memory-address)
                                          :for printedp := (gethash interpretation printed-table)
                                        :while printedp
                                        :finally (return-from nil
                                                   (and (not printedp) interpretation)))))
                                (cond ((null interpretation) nil)
                                      ((< 0 previous-interpretation-size)
                                       ;; then we're blocked
                                       nil)
                                      (t
                                       (setf column-empty-p nil)
                                       (display-interpretation stream row-height
                                                               interpretation)
                                       (setf previous-interpretation-size
                                             (cc.id:size (cc.id:metaobject interpretation)))
                                       (setf (gethash interpretation printed-table)
                                             t))))
                          :when (< 0 previous-interpretation-size)
                            :do (decf previous-interpretation-size 8))))
                    (current-column-width
                     (rectangle-width (bounding-rectangle column-output-record))))
               (setf column-width
                     (+ column-width current-column-width))
               ;; this is just bodgery for now
               ;; later on we will do proper boxing
               (stream-add-output-record stream column-output-record)
               (replay column-output-record stream))))))

(defmethod nde:get-directory-list ((frame disassembler))
  (find-pane-named frame 'directory-list))

(nde:intern-directory-command disassembler)

;;; we're going to need a machine and memory protocol to be able to abstractly
;;; read instructions from some machine's memory.
(define-disassembler-command (com-disassemble-rom :menu "Disassemble ROM")
    ((instruction-set-name symbol)
     (rom-directory pathname))
  (let ((instruction-set-ref-class (find-class instruction-set-name nil)))
    (when instruction-set-ref-class
      (let* ((set (make-instance instruction-set-ref-class))
             (disassembely (cc.mi:describe-memory set rom-directory)))
        (setf (current-disassembely *application-frame*)
              disassembely)))))

(defun launch ()
  (run-frame-top-level (make-application-frame 'disassembler)))
