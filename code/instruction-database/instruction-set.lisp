#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:copy-cat.instruction-database)

(defclass instruction-set ()
  ((%instructions :initarg :instructions :initform (list)
                  :accessor instructions
                  :documentation "Instructions by composition." )

   (%finalized :initarg :finalized :reader finalized :initform nil :type boolean
               :documentation "Whether the build list has been destroyed and the
actual set created")

   (%arguments :initarg :arguments :accessor arguments :initform (list) :type list)
   (%spec-file :initarg :spec-file :accessor spec-file :type pathname)
   (%instruction-variance :type list)
   (%registers :initarg :registers :accessor registers :initform (list) :type list ; not finished
               :documentation "It's not decided how to represent register arrays yet.")
   (%name :initarg :name :reader name :type string)))

(defgeneric add-instruction (instruction set)
  (:method ((instruction instruction) (set instruction-set))
    (push instruction (instructions set))))

;;; now we can generate the dispatch function by normalising the constant-masks of
;;; each instruction.
;;; not i'm not sure how we do that to take into acount endiness but we'll worry later

(defgeneric instruction-variance (instruction-set)
  (:documentation "Set => (values biggest-instruction-size smallest-instruction-size)")
  (:method ((instruction-set instruction-set))
    (with-accessors ((instructions instructions)) instruction-set
      (if (finalized instruction-set)
          (values-list (slot-value instruction-set '%instruction-variance))
          (instruction-variance instructions))))
  (:method ((instructions list))
    (unless (first instructions)
      (return-from instruction-variance nil))
    (let ((biggest (size (first instructions)))
            (smallest (size (first instructions))))
        (loop :for instruction :in instructions
           :do (let ((size (size instruction)))
                 (cond ((> biggest size)
                        (setf biggest size)) 
                       ((< smallest size)
                        (setf smallest size)))))
        (values biggest smallest))))

(defgeneric composition-mask (instructions)
  (:documentation "
The logand of all normalised constant-masks in the list of  instructions
provided.")
  (:method ((instructions list))
    (let ((biggest (instruction-variance instructions)))
      (let ((mask (normalize-composition (first instructions) biggest)))
        (dolist (instruction (rest instructions))
          (setf mask (logand mask (normalize-composition instruction biggest))))
        mask)))
  (:method ((instruction-set instruction-set))
    (composition-mask (instructions instruction-set))))

(defun list-of-bits (integer)
  "https://lisptips.com/post/44261316742/how-do-i-convert-an-integer-to-a-list-of-bits
thanks xach."
  (let ((bits '()))
    (dotimes (index (integer-length integer) bits)
      (push (if (logbitp index integer) 1 0) bits))
    bits))

(defun %possible-byte-specs (mask)
  "Finds all byte-specs but see the clear method because
(4 . 12) should also mean (1 .12), (2 . 12), (3 . 12)
but also (1 . 13), (2 . 13) (1 . 14) ... "
       (let ((bits (list-of-bits mask))
             (byte-spec-list '()))
         (loop :for bit :in bits
            :with position := (length bits)
            :with current-count := 0
            :do (cond ((and (= 0 bit)
                            (not (= 0 current-count)))
                       (push (byte current-count position)
                             byte-spec-list)
                       (setf current-count 0))

                      ((not (= 0 bit))
                       (incf current-count)))
              (decf position)
            :finally (unless (= 0 current-count)
                       (push (byte current-count position)
                             byte-spec-list)))
         byte-spec-list))

(defun possible-byte-specs (mask)
  (let ((specs (%possible-byte-specs mask)))
    (loop :for (spec-size . byte-position) :in specs
       :append (loop :for next-position :from byte-position
                  :below (+ byte-position spec-size)
                    :for size-limit :downfrom spec-size :above 0
                  :append (loop  :for size :downfrom size-limit :above 0
                             :collect (byte size next-position))))))

(defun find-diverging-constant-specs (instructions)
  "find ways to split the instructions."
  (let ((byte-spec-instruction-alist '()))
    (dolist (instruction instructions)
      (let ((byte-specs (possible-byte-specs (constant-mask instruction))))
        (dolist (spec byte-specs)
          (let ((entry (assoc spec byte-spec-instruction-alist :test #'equal)))
            (if entry
                (push instruction (cdr entry))
                (push (list spec instruction) byte-spec-instruction-alist))))))
    (if (null byte-spec-instruction-alist)
        (error "There is no way to split ~a" instructions)
        byte-spec-instruction-alist)))

(defun find-diverging-constants (instructions)
  (let ((diverging-constant-spec-instructions-alist
         (find-diverging-constant-specs instructions))
        (next-list '()))
    (dolist (set diverging-constant-spec-instructions-alist)
      (let ((constant-value-instruction-alist '())
            (segment (car set))
            (variance (instruction-variance (rest set))))
        (dolist (instruction (rest set))
          (let ((instruction-segment
                 (ldb segment
                      (normalize-composition instruction variance
                                             #'constant-value))))
            (let ((entry (assoc instruction-segment constant-value-instruction-alist
                                :test #'equal)))
              (if entry
                  (push instruction (cdr entry))
                  (push (list instruction-segment instruction) constant-value-instruction-alist)))))
        (push (cons segment constant-value-instruction-alist) next-list)))
    ;; we now need to remove entries that (only have 1 constant value and
    ;; the entire list of instructions)
    (loop :for entry :in next-list
       :append (if (and (= 1 (length (rest entry)))
                        (= (length instructions) (length (rest (car (rest entry))))))
                   nil
                   (list entry)))))

(defun pick-diverging-constant (instructions)
  (let ((constant-alist (find-diverging-constants instructions)))
    (first (sort ;; most branches in this branch, least branches in the next one.
            constant-alist
            (lambda (a b)
              (let ((a-score (length a))
                    (b-score (length b)))
                (mapc (lambda (instruction-list)
                        (decf a-score (length instruction-list)))
                      a)
                (mapc (lambda (instruction-list)
                        (decf b-score (length instruction-list)))
                      b)
                (< a-score b-score)))
            :key #'rest))))

(defun diverging-constant-branch (instructions)
  "Turns the sorted pick-diverging-constant tree into one where
it has (t . instructions)"
  (let* ((constant-branch (pick-diverging-constant instructions))
         (diverged-instructions
          (reduce #'append (rest constant-branch) :key #'rest))
         (un-diverged-instructions
          (set-difference instructions diverged-instructions :test #'equal)))
    (if un-diverged-instructions
        (append constant-branch
                (list (cons t un-diverged-instructions)))
        constant-branch)))

(defgeneric composition-tree (instructions)
  (:method ((instructions list))
    (let ((diverging-constant-branch (diverging-constant-branch instructions)))
      (dolist (next-branch (rest diverging-constant-branch))
        (when (< 2 (length next-branch))
          (setf (cdr next-branch)
                (composition-tree (cdr next-branch)))))
      diverging-constant-branch))
  (:method ((instruction-set instruction-set))
    (composition-tree (instructions instruction-set))))
