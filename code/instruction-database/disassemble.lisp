#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:copy-cat.instruction-database)

(defun read-u16 (fast-io-buffer)
  (let ((u 0))
    (setf (ldb (byte 8 8) u)
          (fast-io:readu8 fast-io-buffer))
    (setf (ldb (byte 8 0) u)
          (fast-io:readu8 fast-io-buffer))
    u))

(defun write-mnemonic-and-args (instruction stream)
  (with-accessors ((instruction-metaobject metaobject)
                   (raw-instruction raw-instruction))
      instruction
    (format stream "~6a" (mnemonic instruction-metaobject))
    (let ((first t))
        (loop :for (spec . argument-metaobject) :in (arguments instruction-metaobject)
           :do (progn
                 (format stream "~:[, ~;~]" first)
                 (format stream "~a~a" (mnemonic argument-metaobject)
                         (ldb spec raw-instruction))
                 (setf first nil))))))

(defgeneric write-disassembled-instruction (instruction stream)
  (:documentation "Used by disassemblers or debuggers to print
a peice of memory.")
  (:method ((instruction standard-instruction) stream)
    (with-accessors ((raw-instruction raw-instruction)
                     (instruction-metaobject metaobject))
        instruction
      (format stream "~4,'0X  ~6a" (constant-value instruction-metaobject)
              (mnemonic instruction-metaobject))
      (let ((first t))
        (loop :for (spec . argument-metaobject) :in (arguments instruction-metaobject)
           :do (progn
                 (format stream "~:[, ~;~]" first)
                 (format stream "~a~a" (mnemonic argument-metaobject)
                         (ldb spec raw-instruction))
                 (setf first nil))))))
  (:method ((instruction (eql nil)) stream)
    (format stream "~& UKNOWN INSTRUCTION BLEH FIXME")))

(defun disassemble-instruction (standard-instruction-set raw-instruction stream)
  (let* ((instruction-set (metaobject standard-instruction-set))
         (instruction (dispatch standard-instruction-set raw-instruction))
         (print-column  (+ (instruction-variance instruction-set) 4)))
    (format stream "~&~V,'0b ~Vt"
            (instruction-variance instruction-set)
            raw-instruction print-column)
    (if instruction
        (write-disassembled-instruction instruction stream)
        (format stream "~4,'0X  UNKNOWN" raw-instruction))))

(defgeneric %disassemble-program (instruction-set program)
  (:method ((instruction-set standard-instruction-set) program)
    (with-open-file (in program :element-type '(unsigned-byte 8))
      (fast-io:with-fast-input (b nil in)
        (loop :until (<= (file-length in)
                         (file-position in))
           :collect (dispatch instruction-set (read-u16 b)))))))

;;; the u-16 thing should be infered somehow from the instruction-set
(defgeneric disassemble-program (instruction-set program &optional stream)
  (:method ((instruction-set standard-instruction-set) program &optional s)
    (with-open-file (in program :element-type '(unsigned-byte 8))
      (fast-io:with-fast-input (b nil in)
        (loop :until (<= (file-length in)
                         (file-position in))
           :do (disassemble-instruction instruction-set
                                        (read-u16 b)
                                        s))))))
