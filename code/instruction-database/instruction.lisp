#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#
;;; in this file we use json-clos for reading from alist instruction sets
;;; it's just a convenient way to parse alists into clos without writing readers.
(in-package #:copy-cat.instruction-database)

;;; in addition to effects you might want to have 'reads' which is basically the inverse
(defclass instruction (debug-info)
  ((%composition :initarg :composition :accessor composition :json-key "composition")
   (%arguments :initarg :arguments :accessor arguments :initform (list) :type list
               :documentation "Association list of (byte-spec . argument)")
   (%effects :initarg :effects :accessor effects :initform (list)
                  :type list :documentation "Any registers that the instruction
will effect as a result of its operation that are not included in the arguments.")
   (%size :initarg :size :accessor size :type (unsigned-byte 16))
   (%constant-mask :initarg :constant-mask :accessor constant-mask
                       :documentation"
1 for constant, 0 for argument, unnormalized for size.
e.g. for a composition '((4 2) (\"u12\" 12)) => 1111000000000000")
   (%constant-value :initarg :constant-value :accessor constant-value
                       :documentation "
If there is a constant value for this then put it in the output, otherwise 0
e.g.'((4 2) (\"u12\" 12)) => 0010000000000000

See constant-mask")
   (%finalized :reader finalized :type boolean :initform nil
               :documentation "Whether the composition and instruction
has been finalized"))
  (:metaclass jcsc:json-serializable-class))
(c2mop:finalize-inheritance (find-class 'instruction))

(defgeneric calculate-argument-specs (instruction)
  (:method ((instruction instruction))
    (let ((current-position 0)
          (next-position 0))
      (nreverse
       (loop :for comp :in (reverse (composition instruction))
          :appending
            (progn (setf current-position next-position)
                   (cond ((typep comp 'argument)
                          (incf next-position (size comp))
                          (list (byte (size comp) current-position)))
                         (t (incf next-position (cadr comp))
                            nil))))))))

(defmethod print-object ((instruction instruction) stream)
  (if (slot-boundp instruction '%constant-value)
      (print-unreadable-object (instruction stream :type t :identity t)
        (format stream "~x ~a ~a" (constant-mask instruction)
                (mnemonic instruction)
                (constant-value instruction)))
      (call-next-method)))

(defun composition-component-size (component)
  (cond ((listp component)
         (if (stringp (car component))
             (cadr component)
             (car component)))
        (t (size component))))

(defun composition-component-constant-p (component)
  (and (listp component)
       (not (stringp (car component)))))

(defun calculate-composition-size (composition)
  (let ((size 0))
    (dolist (component composition)
      (incf size (composition-component-size component)))
    size))

(defun calculate-constant-mask (composition size)
  (let ((constant-mask 0)
        (byte-position size))
    (dolist (component composition)
      (let ((component-size (composition-component-size component)))
        (setf byte-position (- byte-position component-size))
        (setf (ldb (byte component-size byte-position)
                   constant-mask)
              (if (composition-component-constant-p component)
                  (1- (expt 2 component-size))
                  0))))
    constant-mask))

(defun composition-component-constant (component)
  (if (composition-component-constant-p component)
      (cadr component)
      0))

(defun calculate-constant-value (composition size)
  (let ((constant-value 0)
        (byte-position size))
    (dolist (component composition)
      (let ((component-size (composition-component-size component)))
        (setf byte-position (- byte-position component-size))
        (setf (ldb (byte component-size byte-position) constant-value)
              (composition-component-constant component))))
    constant-value))

(defun ensure-argument-list (instruction)
  (prog1
      (or (arguments instruction)
          (let* ((argument-specs (calculate-argument-specs instruction))
                 (arguments (loop :for value :in (composition instruction)
                               :appending (cond ((typep value 'argument)
                                                 (list value))))))
            (setf (arguments instruction)
                  (loop :for spec :in argument-specs
                     :for argument :in arguments
                       :collect (cons spec argument)))))))

(defgeneric finalize-composition (instruction)
  (:method ((instruction instruction))
    (setf (size instruction)
          (calculate-composition-size (composition instruction)))
    (setf (constant-mask instruction)
          (calculate-constant-mask (composition instruction)
                                   (size instruction)))
    (setf (constant-value instruction)
          (calculate-constant-value (composition instruction)
                                    (size instruction)))
    (ensure-argument-list instruction)
    (setf (slot-value instruction '%finalized)
          t)
    instruction))

;;; TODO uhhh what's this doing?? this looks very wrong!
(defgeneric normalize-composition (instruction n &optional composition-accessor)
  (:documentation "it's expected n is >= (size instruction)")
  (:method ((instruction instruction) n &optional (composition-accessor #'constant-mask))
    (let ((composition-type (funcall composition-accessor instruction)))
      (if (= n (size instruction))
          composition-type
          (let ((diff (- n (size instruction))))
            (ash composition-type
                 diff))))))

(defmethod describe-object ((instruction instruction) stream)
  (call-next-method)
  (format stream "~&~%~%Arguments and composition:~%~a" (composition instruction))
  (if (stringp (description instruction))
      (format stream "~%~%~a" (description instruction))
      (format stream "~%~{~%~a~}" (description instruction)))
  nil)
