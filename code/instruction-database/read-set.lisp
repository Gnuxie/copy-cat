#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:copy-cat.instruction-database)

(defun read-register (name register-alist)
  (declare (type string name))
  (declare (type list register-alist))
  (let ((register
         (jc.alist:instance<-alist 'register register-alist)))
    (setf (slot-value register '%name) name)
    register))

(defun read-registers (alist-spec)
  (let ((registers (cdr (assoc "registers" alist-spec :test #'string=))))
    (loop :for reg :in registers
               :collect (read-register (car reg) (cdr reg)))))

(defun read-argument (name argument)
  (declare (type string name))
  (declare (type list argument))
  (let ((internal-argument
         (jc.alist:instance<-alist 'argument argument)))
    (setf (slot-value internal-argument '%name) name)
                                        ;    (push internal-argument (arguments set))
    internal-argument))

(defun transform-composition (set composition)
  (mapcar (lambda (c)
            (cond ((stringp c)
                   (let ((argument-isntance (find c (arguments set) :test #'string= :key #'name)))
                     (if argument-isntance
                         argument-isntance
                         (error "Cannot find argment with name ~a in set ~a" c set))))

                  ((and (listp c)
                        (stringp (car c)))
                   (make-instance 'anonymous-argument
                                  :mnemonic (car c)
                                  :size (cadr c)))
                  (t c)))
          composition))

(defun transform-effects (set effects)
  "Instruction set, effects -> effect-isntances"
  (mapcar (lambda (e)
            (etypecase e
              (string
               (let ((register (find e (registers set) :test #'string= :key #'name)))
                 (or register
                     (error "cannot find register with name ~a in set ~a" e set))))))
          effects))

;;; still need to intern arguments into this list in transform composition
;;; maybe the transform composition needs to be move to finalize
(defun read-instruction (set alist-instruction)
  (declare (type instruction-set set))
  (declare (type list alist-instruction))
  (let ((instruction
         (jc.alist:instance<-alist 'instruction alist-instruction)))
    (setf (composition instruction)
          (transform-composition set (composition instruction)))
    (setf (effects instruction)
          (transform-effects set (cdr (assoc "effects" alist-instruction :test #'string=))))
    (finalize-composition instruction)
                                        ;    (add-instruction instruction set)
    instruction))

(defun read-set (alist)
  (declare (type list alist))
  (let ((arguments (cdr (assoc "arguments" alist :test #'string=)))
        (instructions (cdr (assoc "instructions" alist :test #'string=)))
        (set (make-instance 'instruction-set
                            :name (cdr (assoc "set" alist :test #'string=)))))
    (setf (arguments set)
          (loop :for arg :in arguments
             :collect (read-argument (car arg) (cdr arg))))
    (setf (registers set)
          (read-registers alist))
    (dolist (instruction instructions)
      (add-instruction (read-instruction set (cdr instruction))
                       set))
    set))

(defun read-from-file (path)
  (with-open-file (s path)
    (file-position s 1)
    (let ((set (read-set (read s nil))))
      (setf (spec-file set)
            path)
      (setf (slot-value set '%instruction-variance)
            (multiple-value-list (instruction-variance set)))
      (setf (slot-value set '%finalized)
            t)
      set)))
