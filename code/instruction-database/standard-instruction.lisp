#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:copy-cat.instruction-database)

;;; we use this to avoid using the eql specializer against an instruction-set
;;; instead each instruction set will have a class that has a reference to
;;; the instruction-set and to use the instruction set in the api you will
;;; have to make an isntance of such a class.
(defclass metaobject-reference-class (c2mop:standard-class)
  ((%metaobject :initarg :metaobject :reader metaobject)))

(defclass metaobject-reference () ())

(defmethod metaobject ((metaobject metaobject-reference))
  (metaobject (class-of metaobject)))

(defmethod c2mop:validate-superclass ((class c2mop:standard-class)
                                      (super metaobject-reference-class))
  t)

(defmethod c2mop:validate-superclass ((class metaobject-reference-class)
                                      (super c2mop:standard-class))
  t)

(defmethod c2mop:class-direct-superclasses ((class metaobject-reference-class))
  (append (remove (find-class 'standard-object) (call-next-method))
          (list (find-class 'metaobject-reference)
                (find-class 'standard-object))))

;;;
;;; standard-instruction and generation
;;;

(defclass configuration ()
  ((%target-package :initarg :target-package :accessor target-package
                    :type package)))

(defclass standard-instruction ()
  ((%arguments :initarg :arguments :accessor arguments :type list)
   (%raw-instruction :initarg :raw-instruction :reader raw-instruction :type integer)))



;;; defclass form
;;; needs name transformer that is to be used by the instruction-metaobject
;;; to locate the class.

(defgeneric standard-instruction-name<-instruction-metaobject (metaobject config)
  (:method ((instruction-metaobject instruction) (config configuration))
    (intern (concatenate 'string
                         "IX"
                         (let ((*print-base* 16))
                           (princ-to-string (constant-value instruction-metaobject))))
            (target-package config))))

;;; someone else has to make setf the metaobject reference later.
(defgeneric standard-instruction-defclass<-instruction-metaobject
    (instruction-metaobject target-package)
  (:method ((instruction-metaobject instruction) (config configuration))
    (let ((class-name (standard-instruction-name<-instruction-metaobject
                       instruction-metaobject config)))
      `(progn
         (defpackage-plus-1:ensure-export '(,class-name) )
         (defclass ,class-name
             (standard-instruction)
           ()
           (:metaclass metaobject-reference-class))))))

(defgeneric standard-instruction-constructor<-metaobject (metaobject raw-instruction config)
  (:method ((metaobject instruction) raw-instruction (config configuration))
    `(make-instance
      ',(standard-instruction-name<-instruction-metaobject metaobject config)
      :arguments (list ,@ (loop :for (spec . arg) :in (arguments metaobject)
                             :collect `(ldb ',spec ,raw-instruction)))
      :raw-instruction ,raw-instruction)))

;;;
;;; standard-instruction-set
;;;

(defclass standard-instruction-set ()
  ())

(defgeneric standard-instruction-set-name<-metaobject (metaobject config)
  (:method ((metaobject instruction-set) (config configuration))
    (intern (string-upcase (name metaobject)) (target-package config))))

(defgeneric standard-instruction-set-defclass<-metaobject (metaobject config)
  (:method ((metaobject instruction-set) (config configuration))
    (let ((class-name (standard-instruction-set-name<-metaobject metaobject config)))
      `(progn
        (defpackage-plus-1:ensure-export '(,class-name))
        (defclass ,class-name
             (standard-instruction-set)
           ()
          (:metaclass metaobject-reference-class))))))

(defgeneric instruction-apropos (instruction-set instruction-value)
  (:method ((instruction-set-name symbol) instruction-value)
    (let ((instruction-set-ref-class  (find-class instruction-set-name nil)))
      (unless instruction-set-ref-class
        (return-from instruction-apropos nil))
      (let* ((set (make-instance instruction-set-ref-class))
             (instruction (dispatch set instruction-value)))
        (when instruction
          (metaobject instruction))))))

(defmacro with-instruction-arguments (symbols instruction &body body)
  `(destructuring-bind ,symbols (arguments ,instruction)
     ,@body))
