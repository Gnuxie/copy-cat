(asdf:defsystem #:copy-cat.instruction-database
  :author "Gnuxie <Gnuxie@protonmail.com>"
  :license "Cooperative Non-Violent Public License v4+"
  :description ""
  :depends-on ("json-clos" "json-clos.serializer.alist" "fast-io"
                           "defpackage-plus")
  :serial t
  :components ((:file "package")
               (:file "argument")
               (:file "register")
               (:file "instruction")
               (:file "instruction-set")
               (:file "standard-instruction")
               (:file "read-set")
               (:file "composition-tree")
               (:file "disassemble")))
