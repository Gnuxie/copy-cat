#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:copy-cat.instruction-database)

(defgeneric case-statement<-tree (tree keform instruction-transformer)
  (:method (tree keyform instruction-transformer)
    (let ((case-type 'case)
          (byte-spec (first tree)))
      `(,case-type
        (ldb ',byte-spec ,keyform)
        ,@ (loop :for branch :in (rest tree)
              :collect
                (let ((subtree (rest branch)))
                  `(,(car branch)
                     ,(if (= 1 (length subtree))
                          (funcall instruction-transformer (car subtree))
                          (case-statement<-tree subtree keyform instruction-transformer)))))))))

(defgeneric dispatch (instruction-set instruction)
  (:documentation "Find the instruction for the instruction set idk yet."))

(defgeneric dispatch-method<-instruction-set (instruction-set
                                              instruction-set-transformer
                                              instruction-transformer
                                              )
  (:documentation "Create a dispatch method that
the instruction transformer takes the instruction and turns it into a call or soemthing to
put in the case or whatever.")
  (:method ((instruction-set instruction-set) instruction-set-transformer instruction-transformer)
    (let ((composition-tree (composition-tree instruction-set)))
      `(defmethod dispatch ((instruction-set
                             ,(funcall instruction-set-transformer
                                       instruction-set))
                            instruction)
         ,(case-statement<-tree composition-tree 'instruction instruction-transformer)))))

(defun normalize-path (path)
  (etypecase path
    (list (asdf:system-relative-pathname (car path) (cadr path)))
    (string path)
    (pathname path)))

(defmacro define-instruction-set (name spec-path)
  (declare (ignore name))
  (let* ((path (normalize-path spec-path))
         (instruction-set (read-from-file path))
         (config (make-instance 'configuration :target-package *package*))
         (instruction-set-name
          (standard-instruction-set-name<-metaobject instruction-set config)))
    `(progn
       ,(standard-instruction-set-defclass<-metaobject instruction-set config)
       ,@ (loop :for instruction :in (instructions instruction-set)
             :collect (standard-instruction-defclass<-instruction-metaobject
                       instruction config))
       (let ((set (read-from-file ',path)))
         ;; bootstrap metaobject-references in the load env
         (setf (slot-value
                (find-class ',(standard-instruction-set-name<-metaobject
                               instruction-set config))
                '%metaobject)
               set)
         (let ((instructions-to-bind
                ',(loop :for instruction :in (instructions instruction-set)
                     :collect (cons (standard-instruction-name<-instruction-metaobject
                                     instruction config)
                                    (constant-value instruction)))))
           (loop :for (instruction-name . constant-value) :in instructions-to-bind
              :for instruction :in (instructions set)
              :do (progn
                    (assert (= constant-value (constant-value instruction)))
                    (setf (slot-value (find-class instruction-name)
                                      '%metaobject)
                          instruction))))
         ,(dispatch-method<-instruction-set
           instruction-set
           (lambda (i) (declare (ignore i)) instruction-set-name)
           (lambda (i) (standard-instruction-constructor<-metaobject
                        i 'instruction config)))))))
