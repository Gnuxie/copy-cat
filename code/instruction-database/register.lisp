#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:copy-cat.instruction-database)

(defclass register (debug-info)
  ((%size :initarg :size :reader size :type (unsigned-byte 16)
          :documentation "The size of the register in bits"
          :json-key "size")
   (%name :initarg :name :reader name :type string))
  (:metaclass jcsc:json-serializable-class))
(c2mop:finalize-inheritance (find-class 'register))
