#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(defpackage #:copy-cat.instruction-database
  (:use #:cl)
  (:local-nicknames (#:jcsc #:json-clos.mop.serializable-class)
                    (#:jc.alist #:json-clos.serializer.alist))
  (:export
   ;; argument
   #:debug-info
   #:mnemonic
   #:description
   #:argument
   #:size
   #:name
   ;; register
   #:register
   ;; instruction
   #:instruction
   #:composition
   #:arguments
   #:argument-specs
   #:constant-mask
   #:constant-value
   #:finalized
   #:finalize-composition
   ;; instruction-set
   #:instruction-set
   #:instructions
   #:spec-file
   #:instruction-variance
   #:composition-tree
   ;; standard-instruction
   #:mataobject-reference
   #:metaobject-reference-class
   #:metaobject
   #:configuration
   #:target-package
;;; ^^ TODO THESE SHOULD REALLY BE MOVED into a utility package.
   #:standard-instruction
   #:with-instruction-arguments
   ;; read-set
   #:read-from-file
   #:read-register
   #:read-registers
   ;; composition-tree
   #:define-instruction-set
   #:find-instruction-set
   #:normalize-path
   ))
