#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:copy-cat.instruction-database)

(defclass debug-info ()
  ((%mnemonic :initarg :mnemonic
              :reader mnemonic :type string :json-key "mnemonic"
              :documentation "e.g. addr or LD" )

   (%description :initarg :description :reader description
                 :json-key "description" :type (or list string)))
  (:metaclass jcsc:json-serializable-class))

(defclass argument (debug-info)
  ((%size :initarg :size :reader size :type (unsigned-byte 16)
          :documentation "The size of the argument in bits." :json-key "size")

   (%name :initarg :name :reader name :type string))
  (:metaclass jcsc:json-serializable-class))

(c2mop:finalize-inheritance (find-class 'argument))

;;; please note that this is really an instance of a formal argument
;;; if the argument class is to change again to contain context info such as
;;; the position of the argument in an instruction (byte spec) then you must
;;; change it into an argument metaobject and an argument instance.
(defclass anonymous-argument (argument)
  ())

(defmethod print-object ((argument argument) stream)
  (print-unreadable-object (argument stream :identity t :type t)
    (format stream "~a 0x~x" (mnemonic argument) (size argument))))
