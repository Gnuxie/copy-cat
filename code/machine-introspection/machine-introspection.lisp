#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(defpackage #:copy-cat.machine-introspection
  (:use #:cl)
  (:export
   #:describe-memory
   #:memory-address
   #:push-interpretation
   #:interpretations
   #:address
   #:raw-byte))

(in-package #:copy-cat.machine-introspection)

(defgeneric describe-memory (instruction-set/s memory &key start end)
  (:documentation "Provide a vector of memory-address with the interpritations
Memory could equally be a file (ROM) for disassembly

All implementors should expect memory to be a stream of type '(unsigned-byte 8)
for their base method.")
  (:method (instruction-set (memory pathname) &rest args &key &allow-other-keys)
    (with-open-file (s memory :element-type '(unsigned-byte 8))
      (apply #'describe-memory instruction-set s args))))

(defclass memory-address ()
  ((%address :initarg :address :accessor address)
   (%interpretations :initarg :interpretations :reader interpretations
                     :initform (make-array 1 :adjustable t :fill-pointer 0) :type vector
                     :documentation "A list of possible interpritations for the
value in memory, the most possible first. Only interpritations that start from this
address are listed here.")
   (%raw-byte :initarg :raw-byte :accessor raw-byte
              :type (unsigned-byte 8))))
;;; keep only primary references instead
;;; push seen to a hash table
;;; done.

(defgeneric push-interpretation (interpretation memory-address)
  (:method (interpretation (memory-address memory-address))
    (vector-push-extend interpretation (interpretations memory-address) 1)))

(defmethod print-object ((address memory-address) stream)
  (print-unreadable-object (address stream :type t :identity t)
    (format stream "~X, ~a interpritation~:p" (address address)
            (length (interpretations address)))))

;;; what i wanted from this was
;;; addr1 | - - - - - |
;;; addr2 | LD R1 45  |
;;; addr3 | - - - - - |
;;; something like this ^ to be able to be done
;;; with CLIM, but we can't do that without generating a parser
;;; that reads byte by byte, which we don't have with our current 'dispatch'
;;; function.
;;; so this won't work basically.
