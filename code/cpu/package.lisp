#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(defpackage #:copy-cat.cpu
  (:use #:cl)
  (:local-nicknames (#:cc.id #:copy-cat.instruction-database))
  (:export
   #:abstract-cpu
   #:define-abstract-cpu))
