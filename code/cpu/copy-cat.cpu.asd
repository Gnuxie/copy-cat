(asdf:defsystem #:copy-cat.cpu
  :author "Gnuxie <Gnuxie@protonmail.com>"
  :license "Cooperative Non-Violent Public License v4+"
  :description "CPU utilities"
  :depends-on ("copy-cat.instruction-database" "cl-change-case")
  :serial t
  :components ((:file "package")
               (:file "abstract-cpu")))
