#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:copy-cat.cpu)

(defclass abstract-cpu () ())

(defun normalize-name (name)
  (string-upcase (cl-change-case:param-case name)))

(defgeneric slot-name<-register (register config)
  (:method ((register cc.id:register) (config cc.id:configuration))
    (intern (normalize-name (cc.id:name register)) (cc.id:target-package config))))

(defgeneric slot-initarg<-register (register config)
  (:method ((register cc.id:register) (config cc.id:configuration))
    (intern (normalize-name (cc.id:name register)) :keyword)))

(defgeneric slot-option<-register (register config)
  (:method ((register cc.id:register) (config cc.id:configuration))
    (let ((array
           (cdr
            (assoc "array"
                   (json-clos.mop.serializable-class:additional-properties
                    register)
                   :test #'string=)))
          (slot-name (slot-name<-register register config)))
      (if array
          `(,slot-name
             :initarg ,(slot-initarg<-register register config)
             :type (simple-array (unsigned-byte ,(cc.id:size register)))
             :initform (make-array ,(- (cadr array) (car array))
                                   :initial-element 0
                                   :element-type '(unsigned-byte ,(cc.id:size register)))
             :accessor ,slot-name)
          `(,slot-name
            :initarg ,(slot-initarg<-register register config)
            :type (unsigned-byte ,(cc.id:size register))
            :accessor ,slot-name
            :initform 0)))))

(defgeneric ensure-register-exports (registers config)
  (:method (registers (config cc.id:configuration))
    `(progn
       ,@ (loop :for register :in registers
             :collect `(defpackage-plus-1:ensure-export
                        '(,(slot-name<-register register config)))))))

(defmacro define-abstract-cpu (name spec)
  "Defines a class with name from the registers in the spec"
  (let* ((path (cc.id:normalize-path spec))
         (spec (with-open-file (s path)
                 (file-position s 1)
                 (read s nil)))
         (registers (cc.id:read-registers spec))
         (config (make-instance 'cc.id:configuration :target-package *package*)))
    `(progn
       (defpackage-plus-1:ensure-export '(,name))
       ,(ensure-register-exports registers config)
       (defclass ,name (abstract-cpu)
         ,(loop :for register :in registers
             :collect (slot-option<-register register config))))))
